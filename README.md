
<!-- README.md is generated from README.Rmd. Please edit that file -->

[![build
status](https://gitlab.com/gitlab-com/gitlab-docs/badges/master/build.svg)](https://gitlab.com/birkelbayes/rprp/commits/master)
[![coverage
report](https://gitlab.com/birkelbayes/rprp/badges/master/coverage.svg)](https://gitlab.com/birkelbayes/rprp/commits/master)

# rprp

The goal of Robert’s personal R package (rprp) is to help me improve my
helper scripts.

## Installation

You can install the released version of rprp from
[gitlab](https://gitlab.com/birkelbayes/rprp) with:

``` r
install.packages("devtools")
devtools::install_github("r-lib/devtools")
devtools::install_gitlab("birkelbayes/rprp")
```
