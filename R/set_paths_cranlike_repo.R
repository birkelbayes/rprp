#' Setup paths for local cran-repo fir use with packrat
#'
#' This function sets the paths for a local cran-like repository and sets the url for an online cran-repository.
#' The cranlike-repository is relative to the project root.
#'
#' @param cran_repo regular online cran-repository.
#' @param packrat_cran_repo local cranlike repository relative to project root.
#' @export

set_repo_paths <- function(cran_repo = "https://cran.rstudio.com",
                           packrat_cran_repo = "packrat-cran"){
  packrat_cran_dir <- stringr::str_glue("file://", here::here(packrat_cran_repo))
  options(repos = c("CRAN" = cran_repo,
                    "packrat-cran" = packrat_cran_dir))
}
