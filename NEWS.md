# version 0.2.1

Improve imports/exports. Add yml to rbuildignore.

# version 0.2.0

* Add set_paths_cranlike_repo for easy setup of cranlike repository path relative to project root and url for online cran repo.

# version 0.1.0

* Added keep_first_n_iterations to keep only first n iterations of a sienaBayes object, which might be useful for convergence checks later.
* Added na_omit_partialbayes omits NA from posterior distributions of sienaBayes object and sets nmain to correct value.
* Added traceplot_rsiena for simple traceplots of two sienaBayes objects.
* Added a `NEWS.md` file to track changes to the package.
